interface IGeneratorMap
{
	getMap(): Array<Array<number>>;
}

export { IGeneratorMap };