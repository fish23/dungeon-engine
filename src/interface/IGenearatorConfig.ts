interface IGeneratorConfig
{
	id: string;
	width: number;
	height: number;
	type: string;
	items: number;
}

export { IGeneratorConfig };