/* Each node will have six values: 
 X position
 Y position
 Index of the node's parent in the closed array
 Cost from start to current node
 Heuristic cost from current node to destination
 Cost from start to destination going through the current node
*/	
interface IAStarNode
{
	x: number;
	y: number;
	parentIndex: number;
	g: number;
	h: number;
	f: number;
}

export { IAStarNode };