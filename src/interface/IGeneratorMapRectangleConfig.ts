interface IGeneratorMapRectangleConfig
{
	items                  : number,
	factorRectangle        : number,
	spaceRectangle         : number,
	roomSizeFactorRectangle: number
}

export { IGeneratorMapRectangleConfig };