interface IGeneratorMapCaveConfig
{
	items: number,
	factorCave: number,
	spaceCave: number
}

export { IGeneratorMapCaveConfig };