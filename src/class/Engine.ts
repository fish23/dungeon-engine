import { EngineControlsKeyboard } from './EngineControlsKeyboard';
import { EngineGUI } from './EngineGUI';
import { EngineThree } from './EngineThree';
import { EngineGameMap } from './EngineGameMap';

class Engine
{
	private id        : string;
	private el        : HTMLCanvasElement;

	private gui       : EngineGUI;
	private map       : EngineGameMap;

	private controlsKb: EngineControlsKeyboard;
	private graphics  : EngineThree;

	constructor(id: string)
	{
		let el      = document.getElementById(id);
		if(!el)
		{
			throw Error('Generator: Invalid ID!');
		}
		this.gui      = new EngineGUI(id);
		this.el       = this.gui.getViewCanvas();
		this.id       = id;

		this.startMap();
	}

	public startMap()
	{
		let view      = this.gui.getView();
		this.graphics = new EngineThree(this.el, view[4], view[5]);
		this.map      = new EngineGameMap();
		this.map.start();
		this.prepareGUI();
		this.prepareMapGraphics();
		this.afterControlsDir();
		this.afterControlsMove();

		this.controlsKb = new EngineControlsKeyboard(this);
		this.controlsKb.registerMove(this.afterControlsMove.bind(this));
		this.controlsKb.registerDir(this.afterControlsDir.bind(this));

	}

	public getPosition(): {x: number, y: number, dir: string}
	{
		return this.map.getPosition();
	}

	public getMap(): {board: Array<Array<number>>, points: Array<Array<number>>}
	{
		return this.map.getMap();
	}

	public getMapInfo(): {start: Array<number>, end: Array<number>, points: number, width: number, height: number}
	{
		return this.map.getMapInfo();
	}

	private afterControlsMove(): void
	{
		let position = this.getPosition();
		this.graphics.setPositionMap(position.x, position.y, position.dir);
	}

	private afterControlsDir(): void
	{
		this.setCompas();
	}

	public prepareMapGraphics(): void
	{
		this.graphics.initializeMap(this.map);
	}

	public prepareGUI(): void
	{
		// this.graphics.initializeMap(this.map);
	}

	public setCompas(): void
	{
		let position = this.getPosition();
		let el = this.gui.getCompas();
		el.innerHTML = position.dir.toUpperCase();
		this.graphics.setRotationMap(position.dir);
	}

	public getCamera(): THREE.PerspectiveCamera
	{
		return this.graphics.getCamera();
	}

	public getLight(): THREE.PointLight
	{
		return this.graphics.getLight();
	}

}

export { Engine };