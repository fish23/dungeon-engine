import { Engine } from './Engine';

class EngineControlsKeyboard
{
	private engine  : Engine;
	private position: {x: number, y: number, dir: string};
	private map     : {board: Array<Array<number>>, points: Array<Array<number>>};
	private mapInfo : {start: Array<number>, end: Array<number>, points: number, width: number, height: number};

	private callbackMove: Function;
	private callbackDir : Function;



	constructor(engine: Engine)
	{
		this.engine = engine;
		this.position = this.engine.getPosition();
		this.map      = this.engine.getMap();
		this.mapInfo  = this.engine.getMapInfo();
		this.enableEvent();
	}

	public registerMove(callback: Function): void
	{
		this.callbackMove = callback;
	}

	public registerDir(callback: Function): void
	{
		this.callbackDir = callback;
	}

	private enableEvent(): void
	{
		document.addEventListener('keydown', this.controlsKeyboard.bind(this));
	}

	private disableEvent(): void
	{
		document.removeEventListener('keydown', this.controlsKeyboard.bind(this));
	}

	private controlsKeyboard(event: KeyboardEvent): void
	{
		console.log('controls', event);
		let isMove = false;
		let isDir  = false;
		switch(event.code)
		{
			// case 'PageUp':
			// 	this.camera.position.y += 1;
			// 	break;

			// case 'PageDown':
			// 	this.camera.position.y -= 1;
			// 	break;

			case 'ArrowUp':
			case 'Numpad8':
			case 'KeyW':
				isMove = true;
				this.controlsKeyboardMove(this.position.dir);
				break;

			case 'Numpad1':
				isMove = true;
				let dirStrafeLeft = this.position.dir;
				switch(dirStrafeLeft)
				{
					case 'n':
						dirStrafeLeft = 'w';
						break;
					case 's':
						dirStrafeLeft = 'e';
						break;
					case 'w':
						dirStrafeLeft = 's';
						break;
					case 'e':
						dirStrafeLeft = 'n';
						break;
				}
				this.controlsKeyboardMove(dirStrafeLeft);
				break;

			case 'Numpad3':
				isMove = true;
				let dirStrafeRight = this.position.dir;
				switch(dirStrafeRight)
				{
					case 'n':
						dirStrafeRight = 'e';
						break;
					case 's':
						dirStrafeRight = 'w';
						break;
					case 'w':
						dirStrafeRight = 'n';
						break;
					case 'e':
						dirStrafeRight = 's';
						break;
				}
				this.controlsKeyboardMove(dirStrafeRight);
				break;

			case 'ArrowDown':
			case 'Numpad2':
			case 'KeyS':
				isMove = true;
				let dirDown = this.position.dir;
				switch(dirDown)
				{
					case 'n':
						dirDown = 's';
						break;
					case 's':
						dirDown = 'n';
						break;
					case 'w':
						dirDown = 'e';
						break;
					case 'e':
						dirDown = 'w';
						break;
				}
				this.controlsKeyboardMove(dirDown);
				break;

			case 'ArrowLeft':
			case 'Numpad4':
			case 'KeyA':
				isDir = true;
				this.controlsKeyboardDir(true);
				break;

			case 'ArrowRight':
			case 'Numpad6':
			case 'KeyD':
				isDir = true;
				this.controlsKeyboardDir(false);
				break;
		}
		event.preventDefault();
		if(true === isMove) this.callbackMove();
		if(true === isDir) this.callbackDir();
	}

	private controlsKeyboardDir(isLeft: boolean): void
	{
		switch(this.position.dir)
		{
			case 'n':
				this.position.dir = (isLeft) ? 'w' : 'e';
				break;
			case 's':
				this.position.dir = (isLeft) ? 'e' : 'w';
				break;
			case 'w':
				this.position.dir = (isLeft) ? 's' : 'n';
				break;
			case 'e':
				this.position.dir = (isLeft) ? 'n' : 's';
				break;
		}
	}

	private controlsKeyboardMove(dir: string): void
	{
		let possible = true;
		let newPoint: Array<number>;
		 
		switch(dir)
		{
			case 'n':
				if(this.position.y === 1)
				{
					possible = false;
					break;
				}
				newPoint = [this.position.x, this.position.y - 1];
				break;
			case 's':
				if(this.position.y === this.mapInfo.height - 2)
				{
					possible = false;
					break;
				}
				newPoint = [this.position.x, this.position.y + 1];
				break;
			case 'w':
				if(this.position.x === 1)
				{
					possible = false;
					break;
				}
				newPoint = [this.position.x - 1, this.position.y];
				break;
			case 'e':
				if(this.position.x === this.mapInfo.width - 2)
				{
					possible = false;
					break;
				}
				newPoint = [this.position.x + 1, this.position.y];
				break;
		}

		if(false === possible)
		{
			return;
		}

		let boardValue = this.map.board[newPoint[0]][newPoint[1]];
		if(boardValue === 255)
		{
			return;
		}

		this.position.x = newPoint[0];
		this.position.y = newPoint[1];
	}
}

export { EngineControlsKeyboard };