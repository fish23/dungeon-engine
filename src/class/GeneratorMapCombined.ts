import { IGeneratorMapCombinedConfig } from '../interface/IGeneratorMapCombinedConfig';
import { GeneratorMap } from './GeneratorMap';



class GeneratorMapCombined extends GeneratorMap
{
	constructor(width: number, height: number)
	{
		super(width, height);
	}

	public generate(config: IGeneratorMapCombinedConfig)
	{
		this.createCombined(config);
	}
}
export { GeneratorMapCombined }