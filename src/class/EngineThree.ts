import { EngineGameMap } from './EngineGameMap';
import { Utils } from './Utils';

class EngineThree
{
	private el        : HTMLCanvasElement;
	private map       : EngineGameMap;

	private width     : number;
	private height    : number;

	private renderer  : THREE.WebGLRenderer;
	private scene     : THREE.Scene;
	private camera    : THREE.PerspectiveCamera;
	private light     : THREE.PointLight;
	private config    : { shadows: boolean, shadowsHigh: boolean, bump: boolean } = {
		shadows: true, 
		shadowsHigh: true, 
		bump: true
	};
	private geometry  : THREE.BoxGeometry;
	private clock     : THREE.Clock = new THREE.Clock();

	private material  : THREE.MeshNormalMaterial;
	private cubes     : Array<THREE.Mesh> = [];
	private items     : Array<THREE.Mesh> = [];
	private itemsUp   : boolean = true;

	private configCamera: {viewAngle: number, aspect: number, near: number, far: number};

	constructor(el: HTMLCanvasElement, width: number, height: number)
	{
		this.el       = el;
		this.width    = width;
		this.height   = height;
		this.checkDebugConfig();
	}

	private checkDebugConfig(): void
	{
		let shadow = Utils.getParameterByName('graphics.shadow');
		let shadowHigh = Utils.getParameterByName('graphics.shadowHigh');
		let bump = Utils.getParameterByName('graphics.bump');
		if(shadow === '0') this.config.shadows = false;
		if(shadowHigh === '0') this.config.shadowsHigh = false;
		if(bump === '0') this.config.bump = false;
	}

	public initializeMap(map: EngineGameMap)
	{
		this.map = map;
		this.initScene();
		this.createGround();
		this.createSky();
		this.createScene();
		this.render();
		this.gameLoopStart();
		
	}

	private initScene(): void
	{
		this.configCamera = {
			viewAngle: 65,
			aspect: this.width / this.height,
			near: 0.01,
			far: 100
		};
        this.renderer = new THREE.WebGLRenderer({ canvas: this.el, alpha: true, antialias: true });
		this.renderer.shadowMap.enabled = true;
		this.renderer.shadowMap.renderReverseSided = true;
		this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

		this.renderer.gammaInput = true;
		this.renderer.gammaOutput = true;

        this.scene   = new THREE.Scene();
        this.camera  = new THREE.PerspectiveCamera(this.configCamera.viewAngle, this.configCamera.aspect, this.configCamera.near, this.configCamera.far);
        this.scene.add(this.camera);
        this.camera.position.z = 6.15;
        this.camera.position.x = 3;
        this.camera.position.y = 0.05;
		//this.camera.position.y = 1;
        this.renderer.setSize(this.width, this.height);
	}

	private getMaterial(map: string, mapBump: string, bumpScale: number): THREE.MeshPhongMaterial
	{
		let texture  = new THREE.TextureLoader().load( map );
		texture.anisotropy = this.renderer.getMaxAnisotropy();
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		let material: THREE.MeshPhongMaterial;
		if(true === this.config.bump)
		{
			let mapHeight = new THREE.TextureLoader().load( mapBump );
			mapHeight.anisotropy = this.renderer.getMaxAnisotropy();
			mapHeight.wrapS = mapHeight.wrapT = THREE.RepeatWrapping;
			material = new THREE.MeshPhongMaterial( { map: texture, bumpMap: mapHeight, bumpScale: bumpScale } );
		}
		else
		{
			material = new THREE.MeshPhongMaterial( { map: texture } );
		}
		return material;
	}

	public createGround(): void
	{
		let map      = this.map.getMap();
        let size     = 1;
        let geometry = new THREE.BoxGeometry(map.board.length + 30, size, map.board[0].length + 30);
		let texture  = new THREE.TextureLoader().load( '../textures/grounds/ground-dirt1.jpg' );
		texture.anisotropy = this.renderer.getMaxAnisotropy();
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping; 
		texture.repeat.set( Math.round(map.board.length * 0.75), Math.round(map.board[0].length * 0.75) );
		let material: THREE.MeshPhongMaterial;
		if(true === this.config.bump)
		{
			let mapHeight = new THREE.TextureLoader().load( '../textures/grounds/ground-dirt1-bump.jpg' );
			material = new THREE.MeshPhongMaterial( { map: texture, bumpMap: mapHeight, bumpScale: 5 } );
		}
		else
		{
			material = new THREE.MeshPhongMaterial( { map: texture } );
		}
        let cube = new THREE.Mesh(geometry, material);
		cube.position.x = Math.round(map.board.length / 2);
		cube.position.z = Math.round(map.board[0].length / 2);
		cube.position.y = -1;
		if(true === this.config.shadows) cube.receiveShadow = true;
		this.scene.add(cube);
		this.cubes.push(cube);
	}

	public createSky(): void
	{
		let map      = this.map.getMap();
        let size     = 1;
        let geometry = new THREE.BoxGeometry(map.board.length + 30, size, map.board[0].length + 30);
		let texture  = new THREE.TextureLoader().load( '../textures/roofs/roof2.jpg' );
		texture.anisotropy = this.renderer.getMaxAnisotropy();
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping; 
		texture.repeat.set( Math.round(map.board.length / 1.32), Math.round(map.board[0].length / 1.22) );
		let material: THREE.MeshPhongMaterial;
		if(true === this.config.bump)
		{
			let mapHeight = new THREE.TextureLoader().load( '../textures/roofs/roof2-bump.jpg' );
			material = new THREE.MeshPhongMaterial( { map: texture, bumpMap: mapHeight, bumpScale: 0.25 } );
		}
		else
		{
			material = new THREE.MeshPhongMaterial( { map: texture } );
		}
        let cube = new THREE.Mesh(geometry, material);
		cube.position.x = Math.round(map.board.length / 2);
		cube.position.z = Math.round(map.board[0].length / 2);
		cube.position.y = 1;
		this.scene.add(cube);
		this.cubes.push(cube);
	}

	public createScene(): void
	{
		let map      = this.map.getMap();
		let size     = 1;
		let hemi     = new THREE.HemisphereLight( 0x554444, 0x333322, 0.5 );
		this.scene.add( hemi );
        let geometry = new THREE.BoxGeometry(size, size, size);
        let materials: Array<THREE.MeshBasicMaterial> = [];
		for(let i = 1; i <= 5; i++)
		{
			let material = this.getMaterial('../textures/walls/brick' +i + '.jpg', '../textures/walls/brick1-bump.jpg', 0.015);
			materials.push(material);
		}

        for(let x = 0, lenX = map.board.length; x < lenX; x++)
        {
        	let boardY = map.board[x];
        	for(let y = 0, lenY = boardY.length; y < lenY; y++)
        	{
        		if(boardY[y] < 255 && boardY[y] > 0)
        		{
        			let size = 0.15;
					let geometry = new THREE.SphereGeometry( size, 16, 12 );
					let color    = Math.random() * 0xffffff;
					for(let i = 0, len = geometry.faces.length; i < len; i++) geometry.faces[i].color.setHex(color);
					let material = this.getMaterial('../textures/walls/egyptian-hieroglyph2.jpg', '../textures/walls/egyptian-hieroglyph2.jpg', 2.2);
	        		let cube: THREE.Mesh = new THREE.Mesh(geometry, material);
					cube.position.x = x;
					cube.position.z = y;
					cube.position.y = 0;
					if(true === this.config.shadows)
					{
						cube.castShadow = true;
						cube.receiveShadow = true;
					}
					this.scene.add(cube);
					this.items.push(cube);
					if(boardY[y] === 100)
					{
						let spotLight = new THREE.PointLight( 0xffffff, 0.5, 40 );
						spotLight.position.set( x + 0.5, 0.95, y + 0.5 );

						spotLight.decay = 2;
						spotLight.distance = 17;

						if(true === this.config.shadows)
						{
							spotLight.shadowCameraFar = 70;
							spotLight.shadowCameraNear = 0.1;
							spotLight.castShadow = true;
							spotLight.shadow.mapSize.width = (true === this.config.shadowsHigh) ? 1024 : 256;
							spotLight.shadow.mapSize.height = (true === this.config.shadowsHigh) ? 1024 : 256;
							spotLight.shadow.bias = 0.2;
							spotLight.shadow.camera.visible = true;
						}
					    this.light = spotLight;
					    this.scene.add( spotLight );
					}
					continue;
        		}
        		if(boardY[y] < 255) continue;
        		let material = (Math.random() > 0.92) ? materials[Math.floor(Math.random() * materials.length)] : materials[Math.floor(Math.random() * 3)];
        		let cube: THREE.Mesh = new THREE.Mesh(geometry, material);
				cube.position.x = x;
				cube.position.z = y;
				cube.receiveShadow = true;
				this.scene.add(cube);
				this.cubes.push(cube);
        	}
        }
	}


	public setCamera(x: number, y: number, z: number): void
	{
		this.camera.position.x = x;
		this.camera.position.y = y;
		this.camera.position.z = z;
	}

	public setLight(x: number, y: number, z: number): void
	{
		this.light.position.x = x;
		this.light.position.y = y;
		this.light.position.z = z;
	}


	public getCamera(): THREE.PerspectiveCamera
	{
		return this.camera;
	}

	public getLight(): THREE.PointLight
	{
		return this.light;
	}

	public setPositionMap(x: number, y: number, dir: string): void
	{
		this.camera.position.z = y;
		this.camera.position.x = x;
		this.light.position.z = y;
		this.light.position.x = x;
		this.setOffsetLight(dir);
	}

	public setOffsetLight(dir: string): void
	{
		let offset = 0.6;
		switch(dir)
		{
			case 'n':
				this.light.position.x -= offset;
				break;
			case 's':
				this.light.position.x += offset;
				break;
			case 'w':
				this.light.position.z += offset;
				break;
			case 'e':
				this.light.position.z -= offset;
				break;
		}
	}

	public setRotationMap(dir: string): void
	{
		switch(dir)
		{
			case 'n':
				this.camera.rotation.y = 0;
				break;
			case 's':
				this.camera.rotation.y = Math.PI;
				break;
			case 'w':
				this.camera.rotation.y = Math.PI / 2;
				break;
			case 'e':
				this.camera.rotation.y = Math.PI / -2;
				break;
		}
		this.setOffsetLight(dir);
	}

    public render(): void
    {
    	this.animate();
        this.renderer.render(this.scene, this.camera);
    	requestAnimationFrame(this.render.bind(this));
    }

    private animate(): void
    {
    }

    public gameLoopStart(): void
    {
    	setInterval(this.gameLoop.bind(this), 40);
    }

    private gameLoop(): void
    {
    	let delta = this.clock.getDelta(); 
    	if(true === this.itemsUp && this.items[0].position.y > 0.05)
    	{
    		this.itemsUp = false;
    	} 
    	else if(false === this.itemsUp && this.items[0].position.y < -0.26)
    	{
    		this.itemsUp = true;
    	}
    	let newY    = (this.itemsUp === true) ? this.items[0].position.y + (0.07 * delta) : this.items[0].position.y - (0.1 * delta)
    	for(let i in this.items)
    	{
    		this.items[i].position.y = newY;
    		let j = parseInt(i);
    		this.items[i].rotation.y += ((j % 2) === 0) ? (0.2 * delta) : (-0.2 * delta);
    	}


    	if(Math.random() > 0.5) this.light.intensity = 0.25 + (Math.random() * 0.05);
    }

}

export { EngineThree };