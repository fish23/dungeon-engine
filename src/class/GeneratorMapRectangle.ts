import { IGeneratorMapRectangleConfig } from '../interface/IGeneratorMapRectangleConfig';
import { GeneratorMap } from './GeneratorMap';

class GeneratorMapRectangle extends GeneratorMap
{
	constructor(width: number, height: number)
	{
		super(width, height);
	}

	public generate(config: IGeneratorMapRectangleConfig)
	{
		this.createRectangle(config);
	}
}
export { GeneratorMapRectangle }