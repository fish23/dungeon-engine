import { AStar } from './AStar';
import { Draw } from './Draw';

import { GeneratorMapCave } from './GeneratorMapCave';
import { GeneratorMapRectangle } from './GeneratorMapRectangle';
import { GeneratorMapCombined } from './GeneratorMapCombined';

import { IGeneratorMapCaveConfig } from '../interface/IGeneratorMapCaveConfig';
import { IGeneratorMapRectangleConfig } from '../interface/IGeneratorMapRectangleConfig';
import { IGeneratorMapCombinedConfig } from '../interface/IGeneratorMapCombinedConfig';

class Generator
{
	private id        : string;
	private idCanvas  : string;
	private el        : HTMLElement;	
	private width     : number;
	private height    : number;

	private pixelSize : number = 20;

	private board     : Array<Array<number>>;
	private draw      : Draw;

	constructor(id: string, width: number, height: number)
	{

		let el      = document.getElementById(id);
		if(!el)
		{
			throw Error('Generator: Invalid ID!');
		}
		this.el       = el;
		this.id       = id;
		this.idCanvas = id + '-canvas';
		this.width    = width;
		this.height   = height;
		this.createHTML();
		this.draw   = new Draw(this.idCanvas);

		let mapGenerator = new GeneratorMapCombined(this.width, this.height);
		let mapCombinedConfig: IGeneratorMapCombinedConfig = {
			items: 10,
			factorCave: 0.2,
			spaceCave: 10,

			rectangleRatio: 0.5,

			factorRectangle: 0.3,
			spaceRectangle: 12,
			roomSizeFactorRectangle: 0.11,
		}
		mapGenerator.generate(mapCombinedConfig);
		this.board = mapGenerator.getBoard();
		this.refresh();
	}

	private createHTML(): void
	{
		let widthPixels    = this.width * this.pixelSize;
		let heightPixels   = this.height * this.pixelSize;
		let canvasHTML     = `<canvas id="${this.idCanvas}" width="${widthPixels}" height="${heightPixels}"></canvas>`;
		let controlsHTML   = `<div id="controls">`;
		
		controlsHTML      += `<label>factorRectangle</label><input id="factorRectangle" type="text" value="0.3" />`;
		controlsHTML      += `<label>spaceRectangle</label><input id="spaceRectangle" type="text" value="12" />`;
		controlsHTML      += `<label>roomSizeFactorRectangle</label><input id="roomSizeFactorRectangle" type="text" value="0.11" /><br />`;
		controlsHTML      += `<label>items</label><input id="items" type="text" value="13" />`;
		controlsHTML      += `<label>rectangleRatio</label><input id="rectangleRatio" type="text" value="0.5" />`;
		controlsHTML      += `<div id="MapCombined" style="display:block-inline;border:1px solid #333;padding: 3px; width: 100px;">Refresh</div>`
		controlsHTML      += `</div>`;
		this.el.innerHTML  = canvasHTML;
		this.el.innerHTML += controlsHTML;
		document.getElementById('MapCombined').addEventListener('click', this.processRefreshEvent.bind(this));
	}

	private processRefreshEvent(event: Event)
	{
		let factorRectangle = <HTMLInputElement> document.getElementById('factorRectangle');
		let spaceRectangle = <HTMLInputElement> document.getElementById('spaceRectangle');
		let roomSizeFactorRectangle = <HTMLInputElement> document.getElementById('roomSizeFactorRectangle');
		let rectangleRatio = <HTMLInputElement> document.getElementById('rectangleRatio');
		let items = <HTMLInputElement> document.getElementById('items');

		let factorRectangleValue = parseFloat(factorRectangle.value);
		let spaceRectangleValue = parseInt(spaceRectangle.value);
		let roomSizeFactorRectangleValue = parseFloat(roomSizeFactorRectangle.value);
		let rectangleRatioValue = parseFloat(rectangleRatio.value);
		let itemsValue = parseInt(items.value);

		let mapGenerator = new GeneratorMapCombined(this.width, this.height);
		let mapCombinedConfig: IGeneratorMapCombinedConfig = {
			items: itemsValue,
			factorCave: 0.2,
			spaceCave: 10,

			rectangleRatio: rectangleRatioValue,

			factorRectangle: factorRectangleValue,
			spaceRectangle: spaceRectangleValue,
			roomSizeFactorRectangle: roomSizeFactorRectangleValue,
		}
		console.log('mapCombinedConfig', mapCombinedConfig);
		console.log('this.width', this.width);
		console.log('this.height', this.height);
		mapGenerator.generate(mapCombinedConfig);
		this.board = mapGenerator.getBoard();
		this.refresh();
	}

	public refresh(): void
	{
		console.log('refresh', this);
		let colors: Array<string> = [];
		for(let c = 0; c < 255; c++)
		{
			if(c === 100 || c === 200)
			{
				let color = (c === 100) ? '#ff0000' : '#00ff00';
				colors.push(color);
				continue;
			}
			let parts: Array<string> = [];
			for(let i = 0; i < 3; i++)
			{
				let part = (Math.floor(Math.random() * 160) + 64);
				parts.push(part.toString(16));
			}
			let color = '#' + parts.join('');
			colors.push(color);
		}
		for(let x = 0; x < this.width; x++)
		{
			for(let y = 0; y < this.height; y++)
			{
				let color = colors[this.board[x][y]];
				this.drawPixel(x, y, color);
			}
		}
		this.drawGrid();
	}

	private drawPixel(x: number, y: number, color: string): void
	{
		let pixel = (this.board[x][y] === 255) ? true : false;
		this.draw.setInverse(pixel);
		let xPixel = x * this.pixelSize;
		let yPixel = y * this.pixelSize;
		if(this.board[x][y] > 0 && !pixel)
		{
			this.draw.setColor(color);
		}
		this.draw.box(xPixel, yPixel, xPixel + this.pixelSize, yPixel + this.pixelSize);
	}

	private drawGrid(): void
	{
		this.draw.setColor("#779955");
		for(let y = 0; y < this.height; y++)
		{
			let xPixel = (this.width) * this.pixelSize;
			let yPixel = y * this.pixelSize;
			this.draw.box(0, yPixel, xPixel, 1);
		}

		for(let x = 0; x < this.width; x++)
		{
			let xPixel = x * this.pixelSize;
			let yPixel = (this.height) * this.pixelSize;;
			this.draw.box(xPixel, 0, 1, yPixel);
		}
	}
}

export { Generator };