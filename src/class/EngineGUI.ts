class EngineGUI
{
	private id        : string;
	private el        : HTMLElement;
	private elBorder  : HTMLCanvasElement;
	private elView    : HTMLCanvasElement;
	private elCompas  : HTMLElement;

	private width     : number;
	private height    : number;

	private view      : Array<number>; //[x1, y1, x2, y2, width, height]

	constructor(id: string)
	{
		let el      = document.getElementById(id);
		if(!el)
		{
			throw Error('Generator: Invalid ID!');
		}
		this.refreshSize();
		
		let stylesBorder   = ['position: absolute;', 'z-index: 100000;'];
		let elBorder = this.createCanvas('dungeon-gui-border', this.width, this.height, stylesBorder);

		let stylesView   = ['position: absolute;', 'z-index: 100001;', 'left: ' + this.view[0].toString() + 'px;', , 'top: ' + this.view[1].toString() + 'px;'];
		let elView   = this.createCanvas('dungeon-canvas', this.view[4], this.view[5], stylesView);

		let elCompas = document.createElement('div');
		elCompas.id  = 'dungeon-compas';
		elCompas.innerHTML = 'N';

		el.insertAdjacentElement('beforeend', elBorder);
		el.insertAdjacentElement('beforeend', elView);
		el.insertAdjacentElement('beforeend', elCompas);


		this.id       = id;
		this.el       = el;
		this.elBorder = elBorder;
		this.elView   = elView;
		this.elCompas = elCompas;
	}

	public getViewCanvas(): HTMLCanvasElement
	{
		return this.elView;
	}

	public getCompas(): HTMLElement
	{
		return this.elCompas;
	}

	public getView(): Array<number>
	{
		return this.view;
	}

	private createCanvas(id: string, width: number, height: number, styles: Array<string>): HTMLCanvasElement
	{
		let elBorder = document.createElement('canvas');
		elBorder.id  = id;
		elBorder.setAttribute('width', width.toString());
		elBorder.setAttribute('height', width.toString());
		elBorder.setAttribute('style', styles.join(''));
		return elBorder;
	}

	public refreshSize(): void
	{
		this.width  = window.innerWidth;
		this.height = window.innerHeight;

		let widthView  = Math.round(this.width * 0.8);
		let heightView = Math.round(this.height * 0.8);

		let widthViewFrom   = 0;
		let heightViewFrom  = Math.round(this.height * 0.05);
		this.view = [widthViewFrom, heightViewFrom, widthView + widthViewFrom, heightView + heightViewFrom, widthView, heightView];
	}
}

export { EngineGUI };