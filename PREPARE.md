# Typescript DEV #

This is evnironment for developement apps in Typescript with tests Mocha and build Gulp

##Installing the build  dependencies

* `sudo npm install -g gulp-cli`

* `npm install typescript gulp gulp-typescript del browserify gulp-typescript-browserify gulp-mocha gulp-browserify run-sequence -g`
* `npm install typescript gulp gulp-typescript del browserify gulp-typescript-browserify gulp-mocha gulp-browserify run-sequence --save-dev`

##Installing the development dependencies

* `npm install mocha chai ts-node -g`
* `npm install mocha chai ts-node --save-dev`

##Installing the type definitions

* `npm install typings -g`
* `npm install typings --save-dev`

* `typings install dt~mocha --global --save`
* `typings install npm~chai --save`

OR

* `npm install @types/chai @types/mocha --save-dev`

##Writing our first TypeScript test

`export function hello() {
  return 'Hello World!';
}

export default hello;`

Let’s write a test in TypeScript, now that we have a function to test:

`import hello from './hello';
import { expect } from 'chai';
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
// import 'mocha';

describe('Hello function', () => {
  it('should return hello world', () => {
    const result = hello();
    expect(result).to.equal('Hello World!');
  });
});`

Running our tests

`{
  "scripts": {
    "test": "mocha -r ts-node/register src/**/test.ts",
  },
}`